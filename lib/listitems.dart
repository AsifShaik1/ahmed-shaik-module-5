import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class ListItems extends StatefulWidget {
  const ListItems({Key? key}) : super(key: key);
  @override
  State<ListItems> createState() => _ListItemsState();
}

class _ListItemsState extends State<ListItems> {
  final Stream<QuerySnapshot> _itemList =
      FirebaseFirestore.instance.collection("items").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _fNameFieldCtrl = TextEditingController();
    TextEditingController _lNameFieldCtrl = TextEditingController();
    TextEditingController _cityFieldCtrl = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("items")
          .doc(docId)
          .delete()
          .then((value) {
        //print("$docId was deleted.");
      });
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("items");
      _fNameFieldCtrl.text = data["First Name"];
      _lNameFieldCtrl.text = data["Last Name"];
      _cityFieldCtrl.text = data["City"];

      showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: const Text("Update Details"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text("First Name: "),
              TextField(
                controller: _fNameFieldCtrl,
              ),
              const Text("Last Name: "),
              TextField(
                controller: _lNameFieldCtrl,
              ),
              const Text("City: "),
              TextField(
                controller: _cityFieldCtrl,
              ),
              TextButton(
                  onPressed: () {
                    collection.doc(data['doc_id']).update({
                      "First Name": _fNameFieldCtrl.text,
                      "Last Name": _lNameFieldCtrl.text,
                      "City": _cityFieldCtrl.text,
                    });
                    Navigator.pop(context);
                  },
                  child: const Text("Update")),
            ],
          ),
        ),
      );
    }

    return StreamBuilder(
      stream: _itemList,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text("There was an error retrieving records.");
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return ListView(
            shrinkWrap: true,
            children: snapshot.data!.docs
                .map((DocumentSnapshot document) {
                  Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                  return Column(children: [
                    Card(
                      child: Column(children: [
                        ListTile(
                          title: Text(
                              data['First Name'] + " " + data['Last Name']),
                          subtitle: Text(data['City']),
                          //onTap: () => print("ListTile"),
                        ),
                        ButtonTheme(
                          child: ButtonBar(
                            children: [
                              OutlinedButton.icon(
                                  onPressed: () {
                                    _update(data);
                                  },
                                  icon: const Icon(Icons.edit),
                                  label: const Text("Edit")),
                              OutlinedButton.icon(
                                  onPressed: () {
                                    _delete(data['doc_id']);
                                  },
                                  icon: const Icon(Icons.delete),
                                  label: const Text("Delete")),
                            ],
                          ),
                        )
                      ]),
                    )
                  ]);
                })
                .toList()
                .cast(),
          );
        } else {
          return const Text("Database is empty!");
        }
      },
    );
  }
}
