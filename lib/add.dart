import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'listitems.dart';

class AddItem extends StatefulWidget {
  const AddItem({Key? key}) : super(key: key);
  @override
  State<AddItem> createState() => _AddItemState();
}

class _AddItemState extends State<AddItem> {
  @override
  Widget build(BuildContext context) {
    TextEditingController firstNameCntlr = TextEditingController();
    TextEditingController lastNameCntlr = TextEditingController();
    TextEditingController cityCntlr = TextEditingController();

    Future _AddItem() {
      final firstName = firstNameCntlr.text;
      final lastName = lastNameCntlr.text;
      final city = cityCntlr.text;

      final ref = FirebaseFirestore.instance.collection("items").doc();

      return ref.set({
        "First Name": firstName,
        "Last Name": lastName,
        "City": city,
        "doc_id": ref.id
      }).then((value) {
        firstNameCntlr.text = "";
        lastNameCntlr.text = "";
        cityCntlr.text = "";
      }).catchError((onError) => log(onError));
    }

    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: firstNameCntlr,
              decoration: InputDecoration(
                hintText: "Enter First Name",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: lastNameCntlr,
              decoration: InputDecoration(
                hintText: "Enter Last Name",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: cityCntlr,
              decoration: InputDecoration(
                hintText: "Enter City",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              _AddItem();
            },
            child: const Text("Add User"),
          ),
        ],
        // This trailing comma makes auto-formatting nicer for build methods.
      ),
      ListItems()
    ]);
  }
}
